//
//  ViewController.swift
//  NineDrogonsCustomer-ios-swift5
//
//  Created by Lin Tun on 2/24/20.
//  Copyright © 2020 interwebbing. All rights reserved

import UIKit

class ViewController: UIViewController {
    
    let backgroundImageView = UIImageView()

    override func viewDidLoad() {
        super.viewDidLoad()
        setBackground()
        getData()
    }

    fileprivate func getData(){
        
       let url = URL(string: "http://216.172.164.24:9600/v1/9dragon/getUserDatabyNrc?member_nrc=12/DaGaYa(N)000000")!
        
        URLSession.shared.dataTask(with: url){(data,response,error) in
            do{
//                print( try JSONDecoder().decode([Users].self, from: data! ))
//                let users = try JSONDecoder().decode([Users].self, from: data!)
//               for user in users{
//                   print ("ID: \(user.email)\nTitle: \(user.gender)")
//                }
                
                let json = try? JSONSerialization.jsonObject(with: data! as Data, options: .mutableContainers)
                print(json)
                
                guard let profileData = (json as AnyObject).value(forKey: "profile")
                                   
                else{
                    return
                    
                }
                print(profileData)
                
            }
            catch {
                
                print ("There finding error")
                
            }
            
        }.resume()
    }
    
    func setBackground() {
        view.addSubview(backgroundImageView)
        backgroundImageView.translatesAutoresizingMaskIntoConstraints = false
        backgroundImageView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        backgroundImageView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        backgroundImageView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        backgroundImageView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        backgroundImageView.image = UIImage(named: "wp")
        view.sendSubviewToBack(backgroundImageView)
    }
}
struct Users: Decodable {
    let member_email : String
    let member_id : String
   // let email:String
}
